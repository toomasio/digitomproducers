using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitomEvents;
using DigitomUtilities;

namespace DigitomProducers
{
    public class GOESpawnObject : GameObjectEvent
    {
        [SerializeField, PrefabRequisite] private GameObject prefab = null;
        [SerializeField] private bool parentToAffected = false;

        protected override void DoAffectedObjectEvent(GameObject affectedObject)
        {
            var spawn = Instantiate(prefab);
            if (parentToAffected && affectedObject != null)
                spawn.transform.SetParent(affectedObject.transform);
        }
    }
}
